CKEditor Videodetector  plugin for Drupal 8
-------------------------------------------

A module that register a plugin which allows you to insert videos 
from Youtube, Vimeo or Dailymotion only pasting an URL or embed code.:
http://ckeditor.com/addon/videodetector


### Installation

Install per normal instructions:
https://www.drupal.org/documentation/install/modules-themes/modules-8

Download library form http://ckeditor.com/addon/videodetector and install 
it in the root libraries folder with "videodetector" folder name.

### Usage

Go to the Text formats and editors settings (/admin/config/content/formats) and
add the YouTube Button to any CKEditor-enabled text format you want.

### Credits

Initial development and maintenance by [Dilip singh](http://akmaurya.com/)
