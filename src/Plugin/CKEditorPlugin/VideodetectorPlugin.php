<?php

namespace Drupal\ckeditor_videodetector\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "VideoDetector Button" plugin.
 *
 * @CKEditorPlugin(
 *   id = "videodetector",
 *   label = @Translation("VideoDetector Plugin")
 * )
 */
class VideoDetectorPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return array(
      'VideoDetector' => array(
        'label' => t('Mediaplagin'),
        'image' => drupal_get_path('module', 'ckeditor_videodetector') . '/js/plugins/videodetector/icons/videodetector.svg',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_videodetector') . '/js/plugins/videodetector/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return array();
  }

}